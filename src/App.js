import { AppBar, Button, IconButton, List, ListItem, MenuItem, Select, Snackbar, TextField, Toolbar, Typography } from '@material-ui/core';
import Axios from 'axios';
import { Fragment, useEffect, useState } from 'react';
import './App.css';
import CloseIcon from '@material-ui/icons/Close';
import { cryptoAPI } from './utils/browser-crypt-web-api'
import { addNewFileToServer, fetchServerPublicKey, getFile, listFiles, updateFileContent } from './utils/files-api';
import { ab2str, str2ab } from './utils/RSA-crypto-methods';






export default function App() {


  const [selectedEncryptionMethod, setSelectedEncryptionMethod] = useState('None');

  const [filesList, setFilesList] = useState([]);
  const [serverIp, setServerIp] = useState('http://localhost');
  const [serverPort, setServerPort] = useState('4000');
  const [targetFileContent, setTargetFileContent] = useState('');
  const [targetFileName, setTargetFileName] = useState('');

  const [newFileName, setNewFileName] = useState('');
  const [newFileContent, setNewFileContent] = useState('');
  const [addFileToggle, setAddFileToggle] = useState(false);
  const [snackBarMsg, setSnackBarMsg] = useState('');





  useEffect(async () => {
    try {

      const serverPublicKey = await fetchServerPublicKey({ ip: serverIp, port: serverPort });
      console.log({ serverPublicKey })

      const instance = new cryptoAPI('RSA');
      const keys = await instance.importKey(serverPublicKey)

      const encrypted = await instance.encrypt(str2ab('Hello Word'));
      console.log({ encrypted })
      const decRes = await Axios.post(`http://localhost:4000/dec`, { payload: ab2str(encrypted) });
      console.log({ decRes })

    } catch (e) {
      console.error(e)
    }

  }, [])


  function connectToServerAndListFiles() {
    listFiles({ ip: serverIp, port: serverPort })
      .then(files => {
        openSnackBar(`Connected To ${serverIp}:${serverPort}  successfully`)
        setFilesList(files)
      })
      .catch(error => {
        console.error(error);
        openSnackBar(`Error at connecting to server`)
      })
  }


  function fetchFile(fileName) {
    getFile({ ip: serverIp, port: serverPort, fileName, encrypt: selectedEncryptionMethod })
      .then(res => {
        setTargetFileContent(res);
        openSnackBar(`File ${fileName} fetched successfully`)
      })
      .catch(error => {
        console.error(error);
        openSnackBar(`Error at fetching ${fileName}`)
      })
  }

  function updateFile() {
    updateFileContent({
      ip: serverIp,
      port: serverPort,
      fileName: targetFileName,
      fileContent: targetFileContent,
      encrypt: selectedEncryptionMethod,
    })
      .then(res => {
        openSnackBar(`File ${targetFileName} updated successfully`)
      })
      .catch(error => {
        console.error(error);
        openSnackBar(`Error at updating file ${targetFileName}`)
      })
  }

  function addNewFile() {
    addNewFileToServer({
      ip: serverIp,
      port: serverPort,
      fileName: newFileName,
      fileContent: newFileContent,
      encrypt: selectedEncryptionMethod,
    })
      .then(res => {
        connectToServerAndListFiles()
        setAddFileToggle(false)
      })
      .catch(error => {
        console.error(error);
        openSnackBar(`Error at add new file ${newFileName}`)
      })
  }

  const [snackBarOpenState, setSnackBarOpenState] = useState(false);

  const openSnackBar = (message) => {
    setSnackBarOpenState(true);
    setSnackBarMsg(message);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setSnackBarOpenState(false);
  };


  return (<>

    <AppBar color="primary" position="relative">
      <Toolbar style={{ display: 'flex', justifyContent: 'space-between' }}>
        <h3>{'Files Server Projects'}</h3>

        <Select
          style={{ background: 'white' }}
          variant="outlined"
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={selectedEncryptionMethod}
          onChange={e => setSelectedEncryptionMethod(e.target.value)}
        >
          <MenuItem value={'None'}>{'None'}</MenuItem>
          <MenuItem value={'AES'}>{'AES Symmetric Encryption'}</MenuItem>
          <MenuItem value={'RSA'}>{'RSA'}</MenuItem>
        </Select>
      </Toolbar>
    </AppBar>


    <AppBar style={{ background: '#e8e8e8', padding: '50px', marginBottom: '20px' }} position="relative">
      <Toolbar>

        <div style={{ width: '100%', display: 'flex', padding: '0 50px' }}>

          <TextField
            value={serverIp}
            onChange={e => setServerIp(e.target.value)}
            variant="outlined"
            placeholder="Server IP"
            style={{ flexGrow: 20, marginRight: '20px' }}
          />

          <TextField
            value={serverPort}
            onChange={e => setServerPort(e.target.value)}
            variant="outlined"
            placeholder="Server PORT"
            style={{ flexGrow: 1 }}
          />

        </div>

        <div>
          <Button color="primary" variant="contained" onClick={connectToServerAndListFiles}>
            {'Connect'}
          </Button>
        </div>


      </Toolbar>
    </AppBar>

    <div style={{ padding: '20px', display: 'grid', gridTemplateColumns: '25% 1fr' }}>

      <div>
        <Typography variant="h4">
          {'Files List:'}
        </Typography>

        <List>


          <ListItem button>
            <Button color="secondary" onClick={() => setAddFileToggle(x => !x)}>
              {addFileToggle ? 'Cancel' : 'Add New File'}
            </Button>
          </ListItem>

          {
            filesList?.map(fileName => <ListItem
              style={{
                color: fileName == targetFileName ? 'red' : 'black'
              }}
              key={fileName} button onClick={() => {
                fetchFile(fileName);
                setTargetFileName(fileName)
              }}>
              {fileName}
            </ListItem>)
          }

        </List>
      </div>


      <div style={{ display: 'flex', flexDirection: 'column', padding: '40px' }}>
        {
          addFileToggle
            ?
            <>
              <TextField
                style={{ marginBottom: '20px' }}
                placeholder="New File Name"
                variant="outlined"
                value={newFileName}
                onChange={e => setNewFileName(e.target.value)}
              />


              <TextField
                style={{ marginBottom: '20px' }}
                placeholder="New File Content"
                multiline
                rows={10}
                variant="outlined"
                value={newFileContent}
                onChange={e => setNewFileContent(e.target.value)}
              />

              <Button color="secondary" variant="contained" onClick={addNewFile}>
                {'Add'}
              </Button>
            </>
            :
            <>
              <TextField
                style={{ marginBottom: '20px' }}
                placeholder="File Content"
                multiline
                rows={10}
                variant="outlined"
                value={targetFileContent}
                onChange={e => setTargetFileContent(e.target.value)}
              />

              <Button color="secondary" variant="contained" onClick={updateFile}>
                {'Update'}
              </Button>
            </>
        }


      </div>

    </div>




    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      open={snackBarOpenState}
      autoHideDuration={6000}
      onClose={handleClose}
      message={snackBarMsg}
      action={
        <Fragment>
          <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
            <CloseIcon fontSize="small" />
          </IconButton>
        </Fragment>
      }
    />
  </>);
}

