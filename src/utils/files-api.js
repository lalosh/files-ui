import Axios from 'axios';
import { AESDecrypt, AESEncrypt, staticSecretKey } from './AES-crypto-methods';


export function listFiles({ ip, port }) {
    return Axios.get(`${ip}:${port}/files`)
        .then(res => {
            if (res.data && res.data.length)
                return res.data;
            else return [];
        })

}


export function getFile({ ip, port, fileName, encrypt }) {
    let params = {};

    if (encrypt == 'AES') {
        params.encrypt = 'AES';
    }


    return Axios.get(`${ip}:${port}/files/${fileName}`, { params })
        .then(res => {
            if (res.data && res.data.fileContent) {

                if (encrypt == 'AES') {
                    return AESDecrypt({ key: staticSecretKey, encryptedPayload: res.data.fileContent })
                }

                return res.data.fileContent;
            }
            else return '';
        })

}

export function updateFileContent({ ip, port, fileName, fileContent, encrypt }) {

    let fileToSend = fileContent;
    const params = {};

    if (encrypt == 'AES') {
        params.encrypt = 'AES';
        fileToSend = AESEncrypt({ key: staticSecretKey, plainPayload: fileToSend })
    }


    return Axios.put(`${ip}:${port}/files/${fileName}`, { fileContent: fileToSend }, { params })
        .then(res => res)

}

export function fetchServerPublicKey({ ip, port }) {
    return Axios.get(`${ip}:${port}/public-key`)
        .then(res => res.data.publicKey.data)
}

export function addNewFileToServer({ ip, port, fileName, fileContent, encrypt }) {

    let fileToSend = fileContent;
    const params = {};

    if (encrypt == 'AES') {
        params.encrypt = 'AES';
        fileToSend = AESEncrypt({ key: staticSecretKey, plainPayload: fileToSend })
    }



    return Axios.post(`${ip}:${port}/files`, { fileName, fileContent: fileToSend }, { params })
        .then(res => res)

}

